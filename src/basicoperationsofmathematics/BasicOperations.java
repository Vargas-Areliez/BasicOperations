/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicoperationsofmathematics;

/**
 *
 * @author Areliez Vargas
 */
public class BasicOperations {
    private int firstNumber;
    private int secondNumber;
    
    public BasicOperations(){
    }
    
    //The method do the addition of two numbers.
    public int addition(int a, int b){
        if (a < 0 || b < 0) {
            return 0;
        }
        return a+b;
    }
    
    public int Subtration(int a, int b){
        if (a < b) {
            return 0;
        }
        return a-b;
    }
}
